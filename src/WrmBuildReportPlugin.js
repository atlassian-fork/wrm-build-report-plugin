const assert = require('assert');
const fs = require('fs');
const path = require('path');
const WrmDependencyAnalyzer = require('@atlassian/atlassian-wrm-dependency-analyzer');
const enrichWithExternalSize = require('./external-size-enricher');
const chalk = require('chalk');

class WrmBuildStatsReportPlugin {
    constructor({ filename, externals, baseUrl, sizeOnly, staticReport, staticReportFilename }) {
        assert(filename, chalk.red('BuildReportPlugin Error: Option [String] "filename" not specified.'));

        if (staticReport) {
            assert(baseUrl, chalk.red('BuildReportPlugin Error: Option [String] "baseUrl" must be specified when [String] "staticReport" option is enabled.'));
        }

        if (externals) {
            assert(baseUrl, chalk.red('BuildReportPlugin Error: Option [String] "baseUrl" must be specified when [Boolean] "externals" option is enabled.'));
        }

        this.filename = filename;
        this.externals = externals;
        this.baseUrl = baseUrl;
        this.staticReport = staticReport;
        this.staticReportFilename = staticReportFilename || 'static-report.html';
        this.sizeOnly = staticReport ? false : sizeOnly; // Disable "sizeOnly" option if "staticReport" is enabled

        if (staticReport && sizeOnly) {
            console.log(`${chalk.yellow('[WRMBuildReportPlugin]:')} "sizeOnly" option is ignored because you enabled "staticReport".\n`);
        }
    }

    getAllAsyncChunks(entryPoint) {
        const seenChunkGroups = new Set();
        const recursivelyGetAllAsyncChunks = chunkGroups => {
            if (chunkGroups.length === 0) {
                return [];
            }

            return chunkGroups
                .filter(cg => {
                    // circuit breaker
                    // dont use a chunk group more than once
                    const alreadySeen = seenChunkGroups.has(cg);
                    seenChunkGroups.add(cg);
                    return !alreadySeen;
                })
                .map(cg => [...cg.chunks, ...recursivelyGetAllAsyncChunks(cg.getChildren())])
                .reduce((acc, val) => acc.concat(val), []);
        };

        // get all async chunks "deep"
        const allAsyncChunks = recursivelyGetAllAsyncChunks(entryPoint.getChildren());

        return this.deduplicate(allAsyncChunks);
    }

    sizeOfChunks(assets, chunks) {
        if (chunks.length === 0) {
            return {
                css: 0,
                js: 0,
                total: 0,
            };
        }

        const chunkSizeByFileTypeFilter = (chunksToSize, filterRegex) =>
            chunksToSize
                .map(c =>
                    c.files
                        .filter(fname => filterRegex.test(fname))
                        .map(fname => assets[fname].size())
                        .reduce((agg, fsize) => agg + fsize, 0)
                )
                .reduce((agg, fsize) => agg + fsize, 0);

        const cssSize = chunkSizeByFileTypeFilter(chunks, /\.css$/);
        const jsSize = chunkSizeByFileTypeFilter(chunks, /\.js$/);

        return {
            css: cssSize,
            js: jsSize,
            total: cssSize + jsSize,
        };
    }

    getModulesOfChunks(chunks) {
        return [].concat(...chunks.map(c => c.getModules()));
    }

    getModulesOfType(chunks, type) {
        return this.getModulesOfChunks(chunks).filter(module => module.constructor.name === type);
    }

    getReasonModulesOfModulesPool(reasons, modulePool) {
        return reasons.filter(reason => modulePool.includes(reason.module));
    }

    getCleanedReasonsOfPool(reasons, pool) {
        return this.getReasonModulesOfModulesPool(reasons, pool)
            .map(reason => reason.module.rawRequest)
            .filter(raw => !!raw);
    }

    deduplicate(arr) {
        return Array.from(new Set(arr));
    }

    getProvidedExternalDependenciesByModule(chunks) {
        return (
            this.getModulesOfType(chunks, 'ProvidedExternalDependencyModule')
                .map(module => ({
                    name: module._request.amd,
                    dependency: module.getDependency(),
                    users: this.deduplicate(
                        this.getCleanedReasonsOfPool(module.reasons, this.getModulesOfChunks(chunks))
                    ),
                }))
                // dedupe
                .reduce((agg, item) => {
                    const alreadyHasDep = agg.some(dep => dep.dependency === item.dependency);
                    if (!alreadyHasDep) {
                        agg.push(item);
                    }
                    return agg;
                }, [])
        );
    }

    getProvidedExternalDependencies(chunks) {
        const modulesByDependency = this.getModulesOfType(chunks, 'ProvidedExternalDependencyModule')
            .map(module => [module.getDependency(), module._request.amd])
            .reduce((agg, [dep, modName]) => {
                agg[dep] = agg[dep] || [];
                agg[dep].push(modName);
                return agg;
            }, {});

        // dedupe
        return Object.entries(modulesByDependency).reduce((agg, [key, value]) => {
            agg[key] = this.deduplicate(value);
            return agg;
        }, {});
    }

    getExternalDependenciesByModule(chunks) {
        return (
            this.getModulesOfType(chunks, 'EmptyExportsModule')
                .map(module => ({
                    dependency: module.getDependency(),
                    users: this.deduplicate(
                        this.getCleanedReasonsOfPool(module.reasons, this.getModulesOfChunks(chunks))
                    ),
                }))
                // dedupe
                .reduce((agg, item) => {
                    const alreadyHasDep = agg.some(dep => dep.dependency === item.dependency);
                    if (!alreadyHasDep) {
                        agg.push(item);
                    }
                    return agg;
                }, [])
        );
    }

    getExternalDependencies(chunks) {
        const deps = this.getModulesOfType(chunks, 'EmptyExportsModule').map(module => module.getDependency());
        return this.deduplicate(deps);
    }

    calculateSizeForChunkGroup(chunkGroup, assets) {
        return this.sizeOfChunks(assets, chunkGroup.chunks);
    }

    getStatsObjectForChunkGroup(chunkGroup, name, assets) {
        const entrySizes = this.calculateSizeForChunkGroup(chunkGroup, assets);
        return {
            name,
            size: entrySizes.total,
            sizeJs: entrySizes.js,
            sizeCss: entrySizes.css,
            externalDependencies: this.getExternalDependencies(chunkGroup.chunks),
            externalDependenciesUsers: this.getExternalDependenciesByModule(chunkGroup.chunks),
            providedExternalDependencies: this.getProvidedExternalDependencies(chunkGroup.chunks),
            providedExternalDependenciesUsers: this.getProvidedExternalDependenciesByModule(chunkGroup.chunks),
            asyncExternalDependencies: this.getExternalDependencies(this.getAllAsyncChunks(chunkGroup)),
            asyncExternalDependenciesUsers: this.getExternalDependenciesByModule(this.getAllAsyncChunks(chunkGroup)),
            asyncProvidedExternalDependencies: this.getProvidedExternalDependencies(this.getAllAsyncChunks(chunkGroup)),
            asyncProvidedExternalDependenciesUsers: this.getProvidedExternalDependenciesByModule(
                this.getAllAsyncChunks(chunkGroup)
            ),
        };
    }

    getStatsObjectForAsyncChunkGroup(chunkGroup, name, assets, parentChunkStats) {
        const chunkStats = this.getStatsObjectForChunkGroup(chunkGroup, name, assets);

        return Object.assign({}, chunkStats, {
            inheritedSize: parentChunkStats.size,
            inheritedSizeJs: parentChunkStats.sizeJs,
            inheritedSizeCss: parentChunkStats.sizeCss,
            inheritedAndOwnSize: chunkStats.size + parentChunkStats.size,
            inheritedAndOwnSizeJs: chunkStats.sizeJs + parentChunkStats.sizeJs,
            inheritedAndOwnSizeCss: chunkStats.sizeCss + parentChunkStats.sizeCss,
        })
    }

    enrichStats(buildStats) {
        if (!this.externals) {
            return Promise.resolve(buildStats);
        }

        return enrichWithExternalSize(buildStats, this.baseUrl);
    }

    finalizeReport(stats) {
        if (!this.sizeOnly) {
            return stats;
        }

        const removeKeys = [
            'externalDependencies',
            'externalDependenciesUsers',
            'providedExternalDependencies',
            'providedExternalDependenciesUsers',
            'asyncExternalDependencies',
            'asyncExternalDependenciesUsers',
            'asyncProvidedExternalDependencies',
            'asyncProvidedExternalDependenciesUsers',
        ];

        return stats.map( stat => {
            for (const key of removeKeys) {
                delete stat[key];
                for(const asyncChunk of stat.asyncChunks) {
                    delete asyncChunk[key];
                }
            }
            return stat;
        });
    }

    apply(compiler) {
        compiler.hooks.done.tapAsync('build report stats', (stats, callback) => {
            const buildStats = [...stats.compilation.entrypoints.entries()].map(([name, entry]) => {
                const chunkStatistics = this.getStatsObjectForChunkGroup(entry, name, stats.compilation.assets);
                const asyncChunkStatistics = entry
                    .getChildren()
                    .map(cg => this.getStatsObjectForAsyncChunkGroup(cg, cg.name || cg.id, stats.compilation.assets, chunkStatistics));
                
                return {
                    ...chunkStatistics,
                    asyncChunks: asyncChunkStatistics,
                };
            });

            this.enrichStats(buildStats).then(stats => {
                const finalReport = this.finalizeReport(stats);

                fs.writeFileSync(this.filename, JSON.stringify(finalReport, null, 2),'utf-8');

                if (!this.staticReport) {
                    return callback();
                }

                WrmDependencyAnalyzer.getStaticReportHTML(stats, this.baseUrl)
                    .then(staticReportHTML => {
                        const reportTargetPath = path.join(process.cwd(), this.staticReportFilename);

                        fs.writeFileSync(reportTargetPath, staticReportHTML, 'utf-8');
                        
                        callback();
                        
                        console.log(`${chalk.yellow('[WRMBuildReportPlugin]:')} Your static report right here: ${chalk.underline(reportTargetPath)}\n`);
                    })
                    .catch(err => {
                        callback();
                        
                        console.log(`${chalk.yellow('[WRMBuildReportPlugin]:')} ${err}\n`);
                    });
            })
        });
    }
}

module.exports = WrmBuildStatsReportPlugin;
